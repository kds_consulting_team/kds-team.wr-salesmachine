# Keboola Salesmachine Writer

Salesmachine helps sales, product and customer success teams instantly convert more leads, accelerate adoption and reduce churn.
The purpose of the Salesmachine writer is to push customer data from Keboola databases into your Salesmachine platform.

## Salesmachine API documentations

[Salesmachine API document](https://docs.salesmachine.io/?python#introduction)
### Obtaining an API  Key
An API key can be obtained by signing into salesmachine.io, and then go to App Setting > API Keys. Each app has its own set of API keys. It is highly recommended to create a separate API key for each application.

## Configurations

Currently, the writer supports writing datapoints into the *Account* and *Contact* endpoints. The requested endpoint will be based on the filename in the input mapping. If the filename in the input mapping does not fit one of the endpoints supported, the writer will fail.

### Example
1. To post records into account endpoint, there needs to be a table named *account.csv* in the input mapping.
2. Since *Event* endpoint is currently not supported, the writer will fail if a *event.csv* is found in the input mapping

On the other hand, it is mandatory to include the required column from the avialable table. The column/value will be used as an unique identifier in Salesmachine. The writer will not execute if required fields are not available.
During posting data rows, if the unique identifier of the requested table is not found in Salesmachine, a new unique identifier will be created; if not, the existing unique identity in Salesmachine will get updated with the requested parameters.

## Required Columns
| Table Name | Column |
|-|-|
| Account | account_uid |
| Contact | contact_uid |

## Recommended Columns
| Table Name | Column |
|-|-|
| Account | name |
| Contact | email, name, account_uid |

