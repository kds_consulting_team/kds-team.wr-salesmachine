'''
kds-team.wr-salesmachine

'''

import logging
import logging_gelf.handlers
import logging_gelf.formatters
import sys
import os
import json  # noqa
from datetime import datetime  # noqa
import pandas as pd
import numpy
import csv

from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa
from salesmachine.client import Client


# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_API_SECRET = '#api_secret'

MANDATORY_PARS = [KEY_API_TOKEN, KEY_API_SECRET]
MANDATORY_IMAGE_PARS = []

# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"

APP_VERSION = '0.0.3'

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:

    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'), port=int(os.getenv('KBC_LOGGER_PORT')))
    logging_gelf_handler.setFormatter(
        logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)

    # remove default logging to stdout
    logger.removeHandler(logger.handlers[0])


# Parameters for Salesmachine
supported_endpoint = ["account", "contact"]
required_header = {
    "account": {
        "required": "account_uid",
        "recommended": ["name"]
    },
    "contact": {
        "required": "contact_uid",
        "recommended": [
            "email",
            "name",
            "account_uid"
        ]
    }
}


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def get_tables(self, tables, mapping):
        """
        Evaluate input and output table names.
        Only taking the first one into consideration!
        mapping: input_mapping, output_mappings
        """
        # input file
        table_list = []
        for table in tables:
            # name = table["full_path"]
            if mapping == "input_mapping":
                destination = table["destination"]
            elif mapping == "output_mapping":
                destination = table["source"]
            table_list.append(destination)

        return table_list

    def structure_params(self, endpoint, row):
        '''
        Structureing params for PUT request
        '''
        row = row.reset_index(drop=True)

        headers = list(row)
        required_info = required_header[endpoint]
        primary_key = required_info["required"]
        if primary_key not in headers:
            logging.error("Required column [{0}] is missing from [{1}]".format(
                required_header[endpoint]["required"], endpoint+'.csv'))
            sys.exit(1)
        else:
            primary_key_value = row[primary_key][0]
            headers.remove(primary_key)
            data = {}
            for header in headers:
                if pd.isnull(row[header][0]) or row[header][0] == '':
                    pass
                else:
                    if type(row[header][0]) == numpy.int64:
                        data[header] = int(row[header][0])
                    else:
                        data[header] = row[header][0]

        return primary_key_value, data

    def valdiate_user_input(self, in_table, params):
        '''
        Validating the inputs from the users
        '''

        # 1 - validating if the user have entered the configuration
        if params == {} or params == '':
            logging.error('Please configure your component.')
            sys.exit(1)

        # 2 - validating if the credentials are input
        if not params.get('#api_token') or not params.get('#api_secret'):
            logging.error('Credentials are missing.')
            sys.exit(1)

        # 3 - ensure there are input tables
        if len(in_table) < 1:
            logging.error('Please configure your input tables.')
            sys.exit(1)

        # 4 - ensure the input tables are supported endpoints
        for table in in_table:
            endpoint = table.split('.csv')[0]
            if endpoint not in supported_endpoint:
                logging.error(f'{endpoint} is not a supported endpoint.')
                sys.exit(1)

        # 5 - ensure all the required columns exist
        for table in in_table:
            endpoint = table.split('.csv')[0]

            with open(DEFAULT_TABLE_SOURCE+table, 'r') as file:
                reader = csv.reader(file)
                header = next(reader)

                if required_header[endpoint]['required'] not in header:
                    logging.error(
                        f'[{endpoint}] required column is missing: {required_header[endpoint]["required"]}')

                file.close()

    def run(self):
        '''
        Main execution code
        '''
        # Get proper list of tables
        in_tables = self.configuration.get_input_tables()
        in_table_names = self.get_tables(in_tables, 'input_mapping')
        logging.info("IN tables mapped: "+str(in_table_names))

        params = self.cfg_params  # noqa

        # validating user inputs
        self.valdiate_user_input(in_table=in_table_names, params=params)

        # input parameters
        api_token = params["#api_token"]
        api_secret = params["#api_secret"]

        # SalesMachine Client
        smclient = Client(api_token, api_secret)

        # Capture failed requests
        failed_requests = {}

        # lopping for each file
        for file in in_table_names:
            endpoint = file.split(".csv")[0]
            failed_requests[endpoint] = []
            logging.info("Processing: [{}]".format(endpoint))

            itr = 1
            log_list = []
            for chunk in pd.read_csv(DEFAULT_TABLE_SOURCE+file, chunksize=1, dtype=str):
                primary_key, data = self.structure_params(endpoint, chunk)

                if endpoint == "account":
                    request_status = smclient.set_account(
                        primary_key, data)
                elif endpoint == "contact":
                    request_status = smclient.set_contact(
                        primary_key, data)

                # Outputting failed entries
                if not bool(request_status[0]):
                    failed_requests[endpoint].append(primary_key)

                log_list.append(primary_key)
                if itr % 10 == 0:
                    logging.info('Processing - {}'.format(log_list))
                    log_list = []
                itr += 1
            if itr % 10 != 0:
                logging.info('Processing - [{}]'.format(log_list))

        for failed_entry in failed_requests:
            if len(failed_requests[failed_entry]) > 0:
                logging.error('List of failed requests for {0}: {1}'.format(
                    failed_entry, failed_requests[failed_entry]))

        logging.info("Salesmachine writer finished")


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
