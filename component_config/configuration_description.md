The writer supports writing datapoints into the *Account* and *Contact* endpoints. The requested endpoint will be based on the filename in the input mapping.

## Required Columns
| Table Name | Column |
|-|-|
| Account | account_uid |
| Contact | contact_uid |

## Recommended Columns
| Table Name | Column |
|-|-|
| Account | name |
| Contact | email, name, account_uid |
